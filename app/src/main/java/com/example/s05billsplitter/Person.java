package com.example.s05billsplitter;

import java.io.Serializable;

// need to implement serializable to pass between activities ???
public class Person implements Serializable {

    String name;
    double percent;
    double payment;

    Person(String name, double percent) {
        this.name = name;
        this.percent = percent;
        this.payment = 0;
    }
}
