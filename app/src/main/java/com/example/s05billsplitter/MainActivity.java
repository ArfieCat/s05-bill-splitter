package com.example.s05billsplitter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements InputRecyclerViewAdapter.ClickListener {
    // hi -bryan
    // hi -ea
    // hello -peter li

    double total;

    // recycler view stuff
    InputRecyclerViewAdapter adapter;
    ArrayList<Person> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // reset variables
        data = new ArrayList<>();
        total = 0;

        setup();
    }

    void setup() {
        // sets the title bar
        ActionBar title = getSupportActionBar();
        if (title != null) {
            title.setCustomView(R.layout.first_title_layout);
            title.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        }

        RecyclerView recyclerView = findViewById(R.id.recycler_input);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new InputRecyclerViewAdapter(this, this, data);
        recyclerView.setAdapter(adapter);

        Button buttonAdd = findViewById(R.id.button_add);
        buttonAdd.setOnClickListener(v -> {
            onAddClick();
        });

        Button buttonDone = findViewById(R.id.button_done);
        buttonDone.setOnClickListener(v -> {
            onDoneClick();
        });

        EditText editTotal = findViewById(R.id.edit_total);
        editTotal.setOnEditorActionListener((v, actionId, event) -> {
            return onEditTotal(v, actionId);
        });

        // CALCULATE THE TOTAL
        // Divide total by peopleCount and cut off at 2 decimal places
        // Add up amount paid by every person and subtract it from the total
        // Remainder will go to the first person
    }

    void onAddClick() {
        // evenly divide the percentage and set everyone to the new one
        double newPercent = 100f / (adapter.getItemCount() + 1);
        for (Person p : data) {
            p.percent = newPercent;
        }
        adapter.notifyItemRangeChanged(0, data.size());
        // add the new person
        data.add(new Person("", newPercent));
        adapter.notifyItemInserted(data.size());
    }

    void onDoneClick() {
        // calculate sum of input percentages
        double percentTotal = 0;
        for (Person p : data) {
            percentTotal += p.percent;
        }

        // debug print statements
        System.out.println("TOTAL: " + total);
        System.out.println("DATA: ");
        for (Person p : data) {
            System.out.println(p.name + ", " + p.percent);
        }
        System.out.println("PERCENT TOTAL: " + percentTotal);

        if (total > 0 && adapter.getItemCount() > 0 && percentTotal >= 99 && percentTotal <= 101) {
            Intent intent = new Intent(this, FinalBillActivity.class);
            // include all the user input
            intent.putExtra("total", total).putExtra("data", data);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please check inputs!", Toast.LENGTH_LONG).show();
        }
    }

    boolean onEditTotal(TextView v, int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            // hide the keyboard
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            v.clearFocus();
            try {
                // currency isn't supposed to be dealt with in floats but it is just too hard :((
                total = Double.parseDouble(v.getText().toString().trim().replace("$", ""));
                if (total > 1000000) {
                    total = 1000000;
                    Toast.makeText(this, "Total cannot exceed 1000000!", Toast.LENGTH_LONG).show();
                }
                String str = "$" + String.format(Locale.US, "%.2f", total);
                v.setText(str);
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Please check total!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onDeleteClick(View v, int position) {
        // delete the person
        data.remove(position);
        adapter.notifyItemRemoved(position);
        // evenly divide the percentage and set everyone to the new one
        double newPercent = 100f / (adapter.getItemCount());
        for (Person p : data) {
            p.percent = newPercent;
        }
        adapter.notifyItemRangeChanged(0, data.size());
    }

    @Override
    public boolean onEditName(TextView v, int actionId, int position) {
        v.clearFocus();
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            data.get(position).name = v.getText().toString().trim();
            adapter.notifyItemChanged(position);
            return true;
        }
        return false;
    }

    @Override
    public boolean onEditPercent(TextView v, int actionId, int position) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            v.clearFocus();
            try {
                data.get(position).percent = Double.parseDouble(v.getText().toString().trim().replace("%", ""));
                adapter.notifyItemChanged(position);
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Please check input!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }
}