package com.example.s05billsplitter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

public class InputRecyclerViewAdapter extends RecyclerView.Adapter<InputRecyclerViewAdapter.ViewHolder> {
    LayoutInflater layoutInflater;
    ClickListener clickListener;
    ArrayList<Person> data;

    public InputRecyclerViewAdapter(Context context, ClickListener clickListener, ArrayList<Person> data) {
        this.layoutInflater = LayoutInflater.from(context);
        this.clickListener = clickListener;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_recycler_input_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person item = data.get(position);
        holder.editName.setText(item.name);
        String str = String.format(Locale.US, "%.2f", item.percent) + "%";
        holder.editPercent.setText(str);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface ClickListener {
        void onDeleteClick(View v, int position);
        boolean onEditName(TextView v, int actionId, int position);
        boolean onEditPercent(TextView v, int actionId, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        EditText editName;
        EditText editPercent;
        ImageButton buttonDelete;

        ViewHolder(View itemView) {
            super(itemView);

            editName = itemView.findViewById(R.id.edit_name);
            editName.setOnEditorActionListener((v, actionId, event) -> {
                return clickListener.onEditName(v, actionId, getAdapterPosition());
            });

            editPercent = itemView.findViewById(R.id.edit_percent);
            editPercent.setOnEditorActionListener((v, actionId, event) -> {
                return clickListener.onEditPercent(v, actionId, getAdapterPosition());
            });

            buttonDelete = itemView.findViewById(R.id.button_delete);
            buttonDelete.setOnClickListener(v -> {
                clickListener.onDeleteClick(v, getAdapterPosition());
            });
        }
    }
}