package com.example.s05billsplitter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

public class FinalRecyclerViewAdapter extends RecyclerView.Adapter<FinalRecyclerViewAdapter.ViewHolder> {
    LayoutInflater layoutInflater;
    ArrayList<Person> data;

    public FinalRecyclerViewAdapter(Context context, ArrayList<Person> data) {
        this.layoutInflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_recycler_final_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Person item = data.get(position);
        holder.textName.setText(item.name);
        String str = "$" + String.format(Locale.US, "%.2f", item.payment);
        holder.textPayment.setText(str);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textName;
        TextView textPayment;

        ViewHolder(View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.text_name);
            textPayment = itemView.findViewById(R.id.text_payment);
        }
    }
}