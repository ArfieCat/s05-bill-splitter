package com.example.s05billsplitter;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FinalBillActivity extends AppCompatActivity {
    double total;

    // recycler view number 2 stuff
    FinalRecyclerViewAdapter adapter;
    ArrayList<Person> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_bill);

        // get user inputs from the last activity
        Intent intent = getIntent();
        total = intent.getDoubleExtra("total", 100);
        data = (ArrayList<Person>) intent.getSerializableExtra("data");

        // calculate money for each person
        // calculate sum of calculated payments
        double sum = 0;
        for (Person p : data) {
            p.payment = Math.round(total * p.percent) / 100.0;
            sum += p.payment;
        }

        // bit of rounding error correction
        data.get(0).payment += total - sum;

        // bit of a hacky way to display the total at the end of the list
        Person totalHolder = new Person("Total:", 100);
        totalHolder.payment = total;
        data.add(totalHolder);

        setup();
    }

    void setup() {
        // sets the title bar
        ActionBar title = getSupportActionBar();
        if (title != null) {
            title.setCustomView(R.layout.second_title_layout);
            title.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        }

        RecyclerView recyclerView = findViewById(R.id.recycler_final);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FinalRecyclerViewAdapter(this, data);
        recyclerView.setAdapter(adapter);

        Button buttonDone = findViewById(R.id.button_done);
        buttonDone.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });
    }
}